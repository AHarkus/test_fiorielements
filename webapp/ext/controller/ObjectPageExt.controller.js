sap.ui.controller("Test2.ext.controller.ObjectPageExt", {

/* Route back to the Main page on click */
	onClickActionProductsHeader1: function (oEvent) {
		var oApi = this.extensionAPI;
		var oNavigationController = oApi.getNavigationController();

		oNavigationController.navigateInternal("", {
			navigationProperty: "Main"
		});
	}
});