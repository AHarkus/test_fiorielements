sap.ui.define(["sap/suite/ui/generic/template/lib/AppComponent"], function (AppComponent) {
	return AppComponent.extend("Test2.Component", {
		metadata: {
			"manifest": "json"
		},

		init: function () {
			// enable routing
			this.getRouter().initialize();
		}

	});
});